@extends('layout')
@section('dashboard-content')

    @if(Session::get('deleted'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('deleted') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(Session::get('delete-failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('delete-failed') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

<!-- Start content -->
<div class="content">
<div class="container">

<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            
            <ol class="breadcrumb p-0">
                <li>
                    <a href="#">Uplon</a>
                </li>
                <li>
                    <a href="#">Tables</a>
                </li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end row -->
<div style="padding:5px;">
    
    <a href="{{URL::to('create-product')}}"> <button type="button" class="btn btn-primary waves-effect waves-light"> New product</button></a> 
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="table-responsive" data-pattern="priority-columns">
                    <table id="tech-companies-1" class="table table-striped table-bordered">
                        <thead class="thead-default">
                            <tr>
                                <th>NO</th>
                                <th data-priority="1">Product Name</th>
                                <th data-priority="3">Price</th>
                                <th data-priority="3">Cost</th>
                                <th data-priority="3">Quentity</th>
                                <th data-priority="3">Discount</th>
                                <th data-priority="3">Hot Product</th>
                                <th data-priority="3">New Arrival</th>
                                <th data-priority="3">Photo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $products)
                            <tr>
                                {{-- <td>{{ $product->id }}</td> --}}
                                <td>{{ $products->pro_name }}</td>
                                <td>{{ $products->price }}</td>
                                <td>{{ $products->cost }}</td>
                                <td>{{ $products->qty }}</td>
                                <td>{{ $products->discount }}</td>
                                <td>{{ $products->is_hot_product }}</td>
                                <td>{{ $products->is_new_arrival }}</td>
                                <td>{{ $products->photo }}</td>
                                <td>
                                    <a href="{{ URL::to('view-product') }}/{{ $products->id }}" class="btn btn-outline-primary"> View </a>
                                    |
                                    <a href="{{ URL::to('edit-product') }}/{{ $products->id }}" class="btn btn-outline-primary"> Edit </a>
                                    |
                                    <a href="{{ URL::to('delete-product') }}/{{ $products->id }}" class="btn btn-outline-danger delete-confirm" onclick="checkDelete()"> Delete </a>
                                </td>
                            </tr>
                            @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
<!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

    <script>
        function checkDelete() {
            var check = confirm('Are you sure you want to Delete this?');
            if(check){
                return true;
            }
            return false;
        }

        $('.delete-confirm').on('click', function (event) {
            debugger;
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
        
    </script>

@stop
