@extends('layout')
@section('dashboard-content')
    <h1> Update category form</h1>

    @if(Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('success') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(Session::get('failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('failed') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    {{-- <form action="{{ URL::to('update-category') }}/{{ $category->id }}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1"> Category name</label>
            <input type="text" class="form-control" value="{{ $category->name }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter category name" name="categoryName">
        </div>

        <button type="submit" class="btn btn-primary"> Update </button>
    </form> --}}

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Category Information</h4>
                        <ol class="breadcrumb p-0">
                            <li>
                                <a href="#">Category</a>
                            </li>
                            <li class="active">
                                Create Category
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                                <form action="{{ URL::to('update-category') }}/{{ $category->id }}" method="post">
                                    @csrf
                                    <fieldset class="form-group">
                                        <label for="exampleInputEmail1">Category name</label>
                                        <input type="text" class="form-control" id="" name="categoryName" value="{{ $category->cate_name }}"
                                               placeholder="Enter category">
                                    </fieldset>
                                    
                                    <fieldset>
                                         <div class="form-group">
                                                <div class="m-t-30">
                                                    <h6 class="m-b-20 text-muted">File browser</h6>
                                                    <label class="file">
                                                        <input type="file" id="file"  name="categoryIcon">
                                                        <span class="file-custom"></span>
                                                    </label>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div><!-- end col -->
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6 m-t-sm-40">
                                    <fieldset>
                                        
                                        <div>
                                            <img src="" id="photo" value="{{$category->icon}}" height="300" width="300" alt="">
                                        </div>
                                    </fieldset>
                                </form>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->

    </div> <!-- content -->

@stop
