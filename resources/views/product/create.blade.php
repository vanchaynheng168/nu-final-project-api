@extends('layout')
@section('dashboard-content')
    <h1> Create product form</h1>

    @if(Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('success') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(Session::get('failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('failed') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Product Information</h4>
                        <ol class="breadcrumb p-0">
                            <li>
                                <a href="#">Product</a>
                            </li>
                            <li class="active">
                                Create Product
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                                <form action="{{url('post-product-form')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for=""> Product name </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Enter product name" name="productName">
                                    </div>
                            
                                    <div class="form-group">
                                        <label for=""> Product price </label>
                                        <input type="number" class="form-control" id="exampleInputEmail1"  placeholder="0.0" name="productPrice">
                                    </div>
                            
                                    <div class="form-group">
                                        <label for=""> Product discount </label>
                                        <input type="number" class="form-control" id="exampleInputEmail1"  placeholder="0.0" name="productDiscount">
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Select product category </label>
                                        <select class="form-control" name="category">
                                            <option> Select </option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"> {{ $category->cate_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    {{-- <fieldset>
                                         <div class="form-group">
                                                <div class="m-t-30">
                                                    <h6 class="m-b-20 text-muted">File browser</h6>
                                                    <label class="file">
                                                        <input type="file" id="file"  name="categoryIcon"  onchange="loadPhoto(event)">
                                                        <span class="file-custom"></span>
                                                    </label>
                                                </div>
                                            </div>
                                    </fieldset> --}}
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div><!-- end col -->
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6 m-t-sm-40">
                               
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Product Photo </label>
                                            <input type="file" class="form-control" name="productPhoto" onchange="loadPhoto(event)">
                                        </div>
                                
                                        <div class="form-group">
                                            <img id="photo" height="100" width="100">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Is Hot Product </label>
                                            <input type="checkbox" name="isHotProduct"/>
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Is New Arrival </label>
                                            <input type="checkbox" name="isNewArrival"/>
                                        </div>
                                 
                                </form>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->

    </div> <!-- content -->

    <script>
        function loadPhoto(event) {
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('photo');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

@stop
