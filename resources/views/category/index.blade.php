@extends('layout')
@section('dashboard-content')

    @if(Session::get('deleted'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('deleted') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(Session::get('delete-failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('delete-failed') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

<!-- Start content -->
<div class="content">
<div class="container">

<div class="row">
    <div class="col-xs-12">
        <div class="page-title-box">
            <h4 class="page-title">Responsive Table</h4>
            
            <ol class="breadcrumb p-0">
                <li>
                    <a href="#">Uplon</a>
                </li>
                <li>
                    <a href="#">Tables</a>
                </li>
                <li class="active">
                    Responsive Table
                </li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end row -->
<div style="padding:5px;">
    
    <a href="{{URL::to('create-category')}}"> <button type="button" class="btn btn-primary waves-effect waves-light"> New Category</button></a> 
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="table-responsive" data-pattern="priority-columns">
                    <table id="tech-companies-1" class="table table-striped table-bordered">
                        <thead class="thead-default">
                            <tr>
                                <th>NO</th>
                                <th data-priority="1">Category Name</th>
                                {{-- <th data-priority="3">Description</th>
                                <th data-priority="3">Description</th>
                                <th data-priority="3">Description</th>
                                <th data-priority="3">Description</th>
                                <th data-priority="3">Description</th> --}}
                                <th data-priority="3">Action</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->cate_name }}</td>
                                <td>
                                    <a href="{{ URL::to('view-category') }}/{{ $category->id }}" class="btn btn-outline-primary"> View </a>
                                    |
                                    <a href="{{ URL::to('edit-category') }}/{{ $category->id }}" class="btn btn-outline-primary"> Edit </a>
                                    |
                                    <a href="{{ URL::to('delete-category') }}/{{ $category->id }}" class="btn btn-outline-danger delete-confirm" onclick="checkDelete()"> Delete </a>
                                </td>
                            </tr>
                            @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
<!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

    <script>
        function checkDelete() {
            var check = confirm('Are you sure you want to Delete this?');
            if(check){
                return true;
            }
            return false;
        }

    </script>

@stop
