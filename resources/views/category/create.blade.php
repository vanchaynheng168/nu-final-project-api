@extends('layout')
@section('dashboard-content')
    <h1> Create category form</h1>

    @if(Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('success') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(Session::get('failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong> {{ Session::get('failed') }} </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Category Information</h4>
                        <ol class="breadcrumb p-0">
                            <li>
                                <a href="#">Category</a>
                            </li>
                            <li class="active">
                                Create Category
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                                <form action="{{url('post-category-form')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <fieldset class="form-group">
                                        <label for="exampleInputEmail1">Category name</label>
                                        <input type="text" class="form-control" id="" name="categoryName"
                                               placeholder="Enter category">
                                    </fieldset>
                                    
                                    <fieldset>
                                         <div class="form-group">
                                                <div class="m-t-30">
                                                    <h6 class="m-b-20 text-muted">File browser</h6>
                                                    <label class="file">
                                                        <input type="file" id="file"  name="categoryIcon"  onchange="loadPhoto(event)">
                                                        <span class="file-custom"></span>
                                                    </label>
                                                </div>
                                            </div>
                                    </fieldset>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div><!-- end col -->
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6 m-t-sm-40">
                                    <fieldset>
                                        
                                        <div>
                                            <img src="" id="photo" height="300" width="300" alt="">
                                        </div>
                                    </fieldset>
                                </form>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->

    </div> <!-- content -->

    <script>
        function loadPhoto(event) {
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('photo');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

@stop
