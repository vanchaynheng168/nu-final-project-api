<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table -> string('pro_name');
            $table -> integer('category_id');
            $table -> integer('brand_id');
            $table -> integer('user_id');
            $table -> double('price');
            $table -> double('cost');
            $table -> double('qty');
            $table -> string('photo');
            $table -> double('discount');
            $table -> boolean('is_hot_product');
            $table -> boolean('is_new_arrival');
            $table -> string('desc');
            $table -> string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
