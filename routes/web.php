<?php

use Illuminate\Support\Facades\Route;

Route::get('dashboard', 'DashboardController@index');
Route::post('post-category-form', 'CategoryController@store');
Route::get('create-category', 'CategoryController@create');
Route::get('all-category', 'CategoryController@index');
Route::get('edit-category/{id}', 'CategoryController@edit');
Route::post('update-category/{id}', 'CategoryController@update');
Route::get('delete-category/{id}', 'CategoryController@destroy');

/// Start Product Route
Route::get('product','ProductController@index');
Route::get('all-product','ProductController@index');
Route::post('post-product-form','ProductController@store');
Route::get('create-product','ProductController@create');
