<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {

    //auth
    Route::post('logout', 'AuthController@logout');

    //set up address for delivery
    Route::get('location', 'Api\AddressController@showAddress');
    Route::post('location', 'Api\AddressController@setAddress');
    

});

    //public route
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');


    Route::get('categories','Api\CategoryController@index');

    //slider show on main page
    Route::get('sliders','Api\SliderController@index');

    //get all brand 
    Route::get('brand', 'Api\BrandController@index');

    //product
    Route::get('get-all-hot-products', 'Api\ProductController@getAllHotProducts');
    Route::get('products', 'Api\ProductController@index');
    Route::get('products/{productId}', 'Api\ProductController@getProductById');

    //get product by brand
    Route::get('products-by-brand/{brandId}', 'Api\ProductController@getProductsByBrandId');

    //relate product by category id
    Route::get('products-by-cate/{categoryId}', 'Api\ProductController@getProductsByCategoryId');

    //temp product add to card
    Route::get('product-to-card', 'Api\TempProductCardController@show');
    Route::post('product-to-card', 'Api\TempProductCardController@addProductToCard');
    Route::delete('product-to-card/{id}', 'Api\TempProductCardController@destroy');

    //process order product but not work😢😢
    Route::patch('product-to-card/{product}', 'Api\TempProductCardController@update');



