<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function register(Request $request) {
        $fields = $request->validate([
            'username'      => 'required|string',
            'phone_number'  => 'required|string|unique:users,phone_number'
        ]);

        $user = User::create([
            'username'      => $fields['username'],
            'phone_number'  => $fields['phone_number'],
            'status'        => '1'
        ]);

        $token = $user -> createToken('myapptoken') -> plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function login(Request $request) {
        $fields = $request->validate([
            'phone_number' => 'required|string'
        ]);

        //check phone number
        $user = User::where('phone_number', $fields['phone_number'])->first();

        $token = $user -> createToken('myapptoken') -> plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function logout(Request $request) {
        $request->user()->currentAccessToken()->delete();

        return [
            'message' => 'Logged out'
        ];
    }
}
