<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Category;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::all();
        return view('product.index',compact('product'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('product.create',compact('categories'));
    }


    public function store(Request $request)
    {
        $product = new Product();
        $product->pro_name = $request->input('productName');
        $product->price = $request->input('productPrice');
        $product->cost = 0;
        $product->qty = 0;
        $product->discount = $request->input('productDiscount');
        $product->photo = "";
        $product->is_hot_product = $request->input('isHotProduct') ? true : false;
        $product->is_new_arrival = $request->input('isNewArrival') ? true : false;
        $product->category_id = $request->input('category');
        $product->user_id = 0;
        $product->status = "AA";
        $product->desc="BB";
        if($product->save()){
            $photo = $request->file('productPhoto');
            if($photo != null){
                $ext = $photo->getClientOriginalExtension();
                $fileName = rand(10000, 50000) . '.' . $ext;
                if($ext == 'jpg' || $ext == 'png'){
                    if($photo->move(public_path(), $fileName)){
                        $product = Product::find($product->id);
                        $product->photo = url('/') . '/' . $fileName;
                        $product->save();
                    }
                }
            }
            return redirect()->back()->with('success', 'Product information inserted successfully!');
        }
        return redirect()->back()->with('failed', 'Product information could not be inserted!');
    }

    public function show(Product $product)
    {
        //
        return Product::find($id);
    }


    public function edit(Product $product)
    {
        //
    }

    public function update(Request $request, Product $product)
    {
        //
    }

    public function destroy(Product $product)
    {
        //
    }
}

    

