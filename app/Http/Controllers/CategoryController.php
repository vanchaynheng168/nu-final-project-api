<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;


class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return view('category.index',compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category ->cate_name = $request->input('categoryName');
        $category->icon = "";
        $category->user_id = 0;
        $category->desc = "Milk";
        $category->status = 1;

        if($category->save()){
            $photo = $request->file('categoryIcon');
            $ext = $photo->getClientOriginalExtension();
            $fileName = rand(10000,50000).'.'.$ext;

            if($ext == 'jpg' || $ext =='png') {
                if($photo -> move(public_path(),$fileName)) {

                    $category = Category::find($category->id);
                    $category -> icon = url('/').'/'.$fileName;
                    $category->save();
                } 
            }
        }
        return redirect()->back()->with('faild','Error Response!!');
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category,$id)
    {
        $category = Category::find($id);
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, Category $category,$id)
    {
        $category = Category::find($id);
        $category->cate_name = $request->input('categoryName');
        $category->icon = "";
        $category->user_id = 0;
        if($category->save()){
            $photo = $request->file('categoryIcon');
            if($photo != null){
                $ext = $photo->getClientOriginalExtension();
                $fileName = rand(10000, 50000) . '.' . $ext;
                if($ext == 'jpg' || $ext == 'png'){
                    if($photo->move(public_path(), $fileName)){
                        $category = Category::find($category->id);
                        $category->icon = url('/') . '/' . $fileName;
                        $category->save();
                    }
                }
            }
            return redirect()->back()->with('success', 'Updated successfully!');
        }
        return redirect()->back()->with('failed', 'Could not update!');
    }

    public function destroy(Category $category,$id)
    {
        if(Category::destroy($id))
        {
            return redirect()->back()->with('deleted', 'Deleted successfully');
        }
        return redirect()->back()->with('delete-failed', 'Could not delete');
    }
}
