<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends Controller
{
    public function showAddress()
    {
        return Address::all();
    }
    public function setAddress(Request $request)
    {
        $request->validate([
            'username'          => 'required',
            'phone_number'      => 'required',
            'location_name'     => 'required',
            'location_point'    => 'required',
            'place'             => 'required',
            'default_address'   => 'required', //defalut 0 || 0 false 1 true
        ]);
        return Address::create($request->all());
    }
}
