<?php

namespace App\Http\Controllers\Api;

use App\Models\TempProductCard;
use App\Models\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class TempProductCardController extends Controller {
    //
    public function addProductToCard(Request $request) {
        $request->validate([
            'product_id'    => 'required',
            'amoung'        => 'required',
            'status'        => 'required'
        ]);
        return TempProductCard::create($request->all());
    }

    public function show()
    {
        $products = Product::join('temp_product_cards', 'products.id', '=', 'temp_product_cards.product_id')
        ->get(['products.*', 'temp_product_cards.*']);
        return [
            'data'      => $products,
            'message'   => 'success',
            'status'    => '200'
        ];
    }

    public function destroy($id)
    {
        $productId = TempProductCard::destroy($id);

        if ($productId != 0) {
            TempProductCard::destroy($id);
            return [
                'message'   => 'Delete success'
            ];
        }else {
            return [
                'id'        => $productId,
                'message'   => 'not success'
            ];
        }
    }

    // public function update(Request $request, $id)
    // {
    //     $product = TempProductCard::find($id);
    //     $product->status = Input::get('status');
    //     $product->save();
    //     return $product;
    // }

    // I want to update status 1 to 2 but not work help check
    public function update(Request $request, TempProductCard $product){
        $product->update([
            'status' => '2',
        ]);
        $this->meesage('message','Customer updated successfully!');
        return redirect()->back();
     }
}
