<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{

    public function index()
    {
        return ProductResource::collection(Product::all());
    }

    public function getAllHotProducts(){
        $hotProducts = Product::where('is_hot_product', 1)->get();
        return ProductResource::collection($hotProducts);
    }
    public function getProductsByCategoryId($categoryId){
        $productsByCategory = Product::where('category_id', $categoryId)->get();
        return ProductResource::collection($productsByCategory);
    }

    public function getProductsByBrandId($brandId)
    {
        $products = Product::where('brand_id', $brandId)->get();
        return [
            'data'      => $products,
            'message'   => 'success',
            'status'    => '200'
        ];
    }

    public function getProductById($id){
        return Product::find($id);
    }
    
}