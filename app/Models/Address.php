<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'mr_mrs',
        'username',
        'phone_number',
        'location_name',
        'location_point',
        'location_detail',
        'place',
        'default_address',
        'photo'
    ]; 
}
