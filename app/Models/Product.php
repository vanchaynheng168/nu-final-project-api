<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['pro_name', 'category_id', 'brand_id', 'price','cost','discount','cost','user_id','photo','is_hot_product','us_new_arrival','desc','status']; 
    
    public function category(){
        return $this->hasOne('App/Models/Category','id','category_id');
    }
}
