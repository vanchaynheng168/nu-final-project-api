<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'cate_name', 'user_id', 'desc','status', 'icon', 'created_at','updated_at'
    ];
}
